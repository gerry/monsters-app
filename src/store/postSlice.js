import { createSlice } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  posts: [],
  postsPerUser: [],
  loading: false,
  error: ''
}

export const fetchPostsPerUser = createAsyncThunk('posts/fetchPostsPerUser', (userId) => {
  return fetch(`https://jsonplaceholder.typicode.com/users/${userId}/posts`)
    .then(response => response.json())
});

// Generates pending, fulfilled and rejected action types
const postSlice = createSlice({
  name: "posts",
  initialState,
  extraReducers: (builder) =>{
    builder.addCase(fetchPostsPerUser.pending, (state) => {
      state.loading = true
    })
    builder.addCase(fetchPostsPerUser.fulfilled, (state, action) => {
      state.loading = false
      state.postsPerUser = action.payload
      state.error = ''
    })
    builder.addCase(fetchPostsPerUser.rejected, (state, action) => {
      state.loading = false
      state.postsPerUser = []
      state.error = action.error.message
    })
  }
})

export default postSlice.reducer;