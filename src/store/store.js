import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./userSlice";
import todoReducer from "./todoSlice";
import postReducer from "./postSlice";

export default configureStore({
  reducer: {
    users: userReducer,
    todos: todoReducer,
    posts: postReducer
  }
})