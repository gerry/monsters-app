import { createSlice } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  todos: [],
  todosPerUser: [],
  loading: false,
  error: ''
}

// Generates pending, fulfilled and rejected action types
export const fetchTodosPerUser = createAsyncThunk('todos/fetchTodosPerUser', (userId) => {
  return fetch(`https://jsonplaceholder.typicode.com/users/${userId}/todos`)
    .then(response => response.json())
})

const todoSlice = createSlice({
  name: "todos",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchTodosPerUser.pending, (state) => {
      state.loading = true
    })
    builder.addCase(fetchTodosPerUser.fulfilled, (state, action) => {
      state.loading = false
      state.todosPerUser = action.payload
      state.error = ''
    })
    builder.addCase(fetchTodosPerUser.rejected, (state, action) => {
      state.loading = false
      state.todosPerUser = []
      state.error = action.error.message
    })
  }
})

export default todoSlice.reducer;