import styled from "styled-components";

export const MonsterContainer = styled.div`
  width: 80%;
  margin: 0 auto;
`

export const MonsterDetails = styled.div`
  display: flex;
  width: 100%;
  border: 1px solid #0ccac4;
  border-radius: 5px;
  color: #222;
  font-size: 24px;
  h2 {
    margin: 0 0 20px;
  }
  .left {
    display: flex;
    flex-direction: column;
    width: 50%;
    padding: 15px;
  }
  img {
    margin: 0 auto;
  }
`

export const MonsterBoxContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin: 15px 0;

  .todo {
    margin: 10px 0;
    font-size: 16px;
  }
}
`

export const MonsterBox = styled.div`
  display: flex;
  flex-direction: column;
  width: 25%;
  min-height: 300px;
  border: 1px solid #0ccac4;
  border-radius: 5px;
  padding: 15px;
  color: #222;
  font-size: 24px;
  h2 {
    margin: 0;
  }
`