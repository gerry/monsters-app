import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUser } from '../../store/userSlice';
import { fetchTodosPerUser } from '../../store/todoSlice';
import { fetchPostsPerUser } from '../../store/postSlice';

import { MonsterContainer, MonsterDetails, MonsterBox, MonsterBoxContainer } from './monster.styles';
import { Img } from 'react-image';

const Monster = () => {
  const params = useParams();
  const dispatch = useDispatch();

  const user = useSelector(state => state.users.user);
  const todosPerUser = useSelector(state => state.todos.todosPerUser)
  const postsPerUser = useSelector(state => state.posts.postsPerUser);

  useEffect(() => {
    if(!user) {
      dispatch(fetchUser(params.monsterId))
    }
    if(todosPerUser.length === 0 || todosPerUser[0].userId != params.monsterId) {
      dispatch(fetchTodosPerUser(params.monsterId))
    }
    if(postsPerUser.length === 0 || postsPerUser[0].userId != params.monsterId) {
      dispatch(fetchPostsPerUser(params.monsterId))
    }
  }, [])

  const getFiveItems = (array) => {
    return array.slice(0, 5);
  }

  return (
    <MonsterContainer>
      <h1>Monster</h1>
      {
        user && (
          <>
            <MonsterDetails>
              <div className='left'>
                <h2>Details</h2>
                <div><strong>Name: </strong> {user.name}</div>
                <div><strong>Username: </strong> {user.username}</div>
                <div><strong>Email: </strong> {user.email}</div>
                <br/>
                <div><strong>Phone: </strong> {user.phone}</div>
                <div><strong>Website: </strong> {user.website}</div>
                <br />
                <div><strong>Company: </strong> {user.company.name}</div>
                <div>{user.company.catchPhrase}</div>
              </div>
              <Img src={`https://robohash.org/${user.email}?set=set2&size=300x200`} loader={`loading...`} />
            </MonsterDetails>
            <MonsterBoxContainer>
              <MonsterBox>
                <h2>Todos</h2>
                {
                  todosPerUser && getFiveItems(todosPerUser).map((todo) => (
                    <div className='todo'>{todo.title}</div>
                  ))
                }
              </MonsterBox>
              <MonsterBox>
                <h2>Posts</h2>
                {
                  postsPerUser && getFiveItems(postsPerUser).map((post) => (
                    <div className='todo'>{post.title}</div>
                  ))
                }
              </MonsterBox>
              <MonsterBox>
                <h2>Albums</h2>
              </MonsterBox>
            </MonsterBoxContainer>
            
          </>
        )
      }
    </MonsterContainer>
  )
}

export default Monster