import styled from "styled-components";
import { Link } from "react-router-dom";

export const CardContainer = styled(Link)`
  display: flex;
  flex-direction: column;
  background-color: #95dada;
  border: 1px solid grey;
  border-radius: 5px;
  padding: 25px;
  cursor: pointer;
  -moz-osx-font-smoothing: grayscale;
  backface-visibility: hidden;
  transform: translateZ(0);
  transition: transform 0.25s ease-out;
  text-decoration: none;
  
  h2 {
    margin: 0;
  }

  &:hover {
    transform: scale(1.05);
  }
`