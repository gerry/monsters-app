import React from "react";
import { CardContainer } from "./card.styles";

const Card = ({ monster }) => {
  return (
    <CardContainer to={`/monsters/${monster.id}`}>
      <img src={`https://robohash.org/${monster.email}?set=set3&size=180x180`} alt="monster" />
      <h2>{monster.name}</h2>
      <p>{monster.email}</p>
    </CardContainer>
  )
}

export default Card;