import React from 'react'
import Card from '../card/card.component';
import { CardListContainer } from './card-list.styles';

const CardList = ({ monsters }) => {
  return (
    <CardListContainer>
      {
        monsters.map((monster, index) => (
          <Card monster={monster} key={index} />
        ))
      }
    </CardListContainer>
  )
}

export default CardList;