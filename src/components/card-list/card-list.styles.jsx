import styled from "styled-components";

export const CardListContainer = styled.div`
  width: 85vw;
  margin: 20px auto;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-gap: 20px;

  @media (max-width: 1200px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
  
  @media (max-width: 992px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (max-width: 650px) {
    grid-template-columns: 1fr;
  }
`
