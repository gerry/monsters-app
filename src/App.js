import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchUsers } from './store/userSlice';

import CardList from './components/card-list/card-list.component';
import SearchBox from './components/search-box/search-box.component';
import './App.css';

const App = () =>  {

  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.users);

  const [searchField, setSearchField] = useState('');

  const filterMonsters = users.filter(monster => monster.name.toLowerCase().includes(searchField.toLowerCase()));

  const handleChange = (e) => {
    setSearchField(e.target.value)
  }

  useEffect(() => {
    dispatch(fetchUsers());
  },[])

  return (
    <div className="App">
      <h1>Monsters Rolodex</h1>
      <SearchBox placeholder="Search Monsters" handleChange={handleChange} />
      <CardList monsters={filterMonsters} />
    </div>
  );
}

export default App;
